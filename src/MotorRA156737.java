import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.stream.Collectors;

/**
 * Motor RA156737
 * 
 * Tratamentos implementados
 * 	ATAQUE
 * 		pega lacaio jogado da cópia oculta
 * 		verifica se tem Funcionalidade PROVOCAR ativa
 * 			se tem lacaio com provocar != lacaio Alvo
 * 		verifica se lacaio já atacou
 * 		verifica se lacaio foi invocado nesse turno e está atacando
 * 		//pegando a cópia oculta do lacaio Alvo
 * 	LACAIO
 * 		verifica se estou invocando uma cartaMagia como Lacaio
 * 		verifica se tem mana o suficiente para realizar a jogada
 * 		verifica se temos 7 lacaios no campo
 * 		verifica se existe a carta na mão
 * 		
 * 	MAGIA
 * 		verifica se estou invocando uma cartaLacaio como Magia
 * 		verifica se tem mana o suficiente para realizar a jogada
 * 		verifica se existe a carta na mão
 * 			ALVO
 * 				verifico se é um lacaio meu	
 * 			AREA
 * 				procura se o alvo é um lacaio do meu campo
 * 			BUFF
 * 				verifica se o alvo é um herói
 * 				procura se o alvo é um lacaio oponente
 * 	PODER
 * 		verifica se o poderHeroico já foi usado
 * 		verifica se tem mana o suficiente para realizar a jogada
 * 		verifica se tem algum lacaio com PROVOCAR
 * 
 * 	
 * 	
 * @author       Matheus Mendes Araujo
 */
public class MotorRA156737 extends Motor {

	public MotorRA156737(Baralho deck1, Baralho deck2, ArrayList<Carta> mao1,
			ArrayList<Carta> mao2, Jogador jogador1, Jogador jogador2,
			int verbose, int tempoLimitado, PrintWriter saidaArquivo, EnumSet<Funcionalidade> funcionalidadesAtivas) {
		super(deck1, deck2, mao1, mao2, jogador1, jogador2, verbose,
				tempoLimitado, saidaArquivo, funcionalidadesAtivas);
		imprimir("========================");
		imprimir("*** Classe "+this.getClass().getName()+" inicializada.");
		imprimir("Funcionalidade ativas no Motor:");
		imprimir("INVESTIDA: "+(this.funcionalidadesAtivas.contains(Funcionalidade.INVESTIDA)?"SIM":"NAO"));
		imprimir("ATAQUE_DUPLO: "+(this.funcionalidadesAtivas.contains(Funcionalidade.ATAQUE_DUPLO)?"SIM":"NAO"));
		imprimir("PROVOCAR: "+(this.funcionalidadesAtivas.contains(Funcionalidade.PROVOCAR)?"SIM":"NAO"));
		imprimir("========================");
	}
	
	private int jogador; // 1 == turno do jogador1, 2 == turno do jogador2.
	private int turno;
	private int nCartasHeroi1;
	private int nCartasHeroi2;
	
	private Mesa mesa;
	
	// "Apontadores" - Assim podemos programar genericamente os métodos para funcionar com ambos os jogadores
	private ArrayList<Carta> mao;
	private ArrayList<Carta> lacaios;
	private ArrayList<Carta> lacaiosOponente;
	
	// "Memória" - Para marcar ações que só podem ser realizadas uma vez por turno.
	private boolean poderHeroicoUsado = false;
	private HashSet<Integer> lacaiosAtacaramID;

	@Override
	public int executarPartida() throws LamaException {
		vidaHeroi1 = vidaHeroi2 = 30;
		manaJogador1 = manaJogador2 = 1;
		nCartasHeroi1 = cartasIniJogador1; 
		nCartasHeroi2 = cartasIniJogador2;
		ArrayList<Jogada> movimentos = new ArrayList<Jogada>();
		int noCardDmgCounter1 = 1;
		int noCardDmgCounter2 = 1;
		turno = 1;
		
		for(int k = 0; k < 60; k++){
			imprimir("\n=== TURNO "+turno+" ===\n");
			// Atualiza mesa (com cópia profunda)
			@SuppressWarnings("unchecked")
			ArrayList<CartaLacaio> lacaios1clone = (ArrayList<CartaLacaio>) UnoptimizedDeepCopy.copy(lacaiosMesa1);
			@SuppressWarnings("unchecked")
			ArrayList<CartaLacaio> lacaios2clone = (ArrayList<CartaLacaio>) UnoptimizedDeepCopy.copy(lacaiosMesa2);
			mesa = new Mesa(lacaios1clone, lacaios2clone, vidaHeroi1, vidaHeroi2, nCartasHeroi1+1, nCartasHeroi2, turno>10?10:turno, turno>10?10:(turno==1?2:turno));
			
			// Apontadores para jogador1
			mao = maoJogador1;
			lacaios = lacaiosMesa1;
			lacaiosOponente = lacaiosMesa2;
			jogador = 1;
			
			// Processa o turno 1 do Jogador1
			imprimir("\n----------------------- Começo de turno para Jogador 1:");
			long startTime, endTime, totalTime;
			
			// Cópia profunda de jogadas realizadas.
			@SuppressWarnings("unchecked")
			ArrayList<Jogada> cloneMovimentos1 = (ArrayList<Jogada>) UnoptimizedDeepCopy.copy(movimentos);
			
			startTime = System.nanoTime();
			if( baralho1.getCartas().size() > 0){
				if(nCartasHeroi1 >= maxCartasMao){
					movimentos = jogador1.processarTurno(mesa, null, cloneMovimentos1);
					comprarCarta(); // carta descartada
				}
				else
					movimentos = jogador1.processarTurno(mesa, comprarCarta(), cloneMovimentos1);
			}
			else{
				imprimir("Fadiga: O Herói 1 recebeu "+noCardDmgCounter1+" de dano por falta de cartas no baralho. (Vida restante: "+(vidaHeroi1-noCardDmgCounter1)+").");
				vidaHeroi1 -= noCardDmgCounter1++;
				if( vidaHeroi1 <= 0){
					// Jogador 2 venceu
					imprimir("O jogador 2 venceu porque o jogador 1 recebeu um dano mortal por falta de cartas ! (Dano : " +(noCardDmgCounter1-1)+ ", Vida Herói 1: "+vidaHeroi1+")");
					return 2;
				}
				movimentos = jogador1.processarTurno(mesa, null, cloneMovimentos1);
			}
			endTime = System.nanoTime();
			totalTime = endTime - startTime;
			if( tempoLimitado!=0 && totalTime > 3e8){ // 300ms
				// Jogador 2 venceu, Jogador 1 excedeu limite de tempo
				return 2;
			}
			else
				imprimir("Tempo usado em processarTurno(): "+totalTime/1e6+"ms");

			// Começa a processar jogadas do Jogador 1
			this.poderHeroicoUsado = false;
            this.lacaiosAtacaramID = new HashSet<Integer>();
			for(int i = 0; i < movimentos.size(); i++){
				processarJogada (movimentos.get(i));
			}
			
			if( vidaHeroi2 <= 0){
				// Jogador 1 venceu
				return 1;
			}
			
			// Atualiza mesa (com cópia profunda)
			@SuppressWarnings("unchecked")
			ArrayList<CartaLacaio> lacaios1clone2 = (ArrayList<CartaLacaio>) UnoptimizedDeepCopy.copy(lacaiosMesa1);
			@SuppressWarnings("unchecked")
			ArrayList<CartaLacaio> lacaios2clone2 = (ArrayList<CartaLacaio>) UnoptimizedDeepCopy.copy(lacaiosMesa2);
			mesa = new Mesa(lacaios1clone2, lacaios2clone2, vidaHeroi1, vidaHeroi2, nCartasHeroi1, nCartasHeroi2+1, turno>10?10:turno, turno>10?10:(turno==1?2:turno));
			
			// Apontadores para jogador2
			mao = maoJogador2;
			lacaios = lacaiosMesa2;
			lacaiosOponente = lacaiosMesa1;
			jogador = 2;
			
			// Processa o turno 1 do Jogador2
			imprimir("\n\n----------------------- Começo de turno para Jogador 2:");
			
			// Cópia profunda de jogadas realizadas.
			@SuppressWarnings("unchecked")
			ArrayList<Jogada> cloneMovimentos2 = (ArrayList<Jogada>) UnoptimizedDeepCopy.copy(movimentos);
			
			startTime = System.nanoTime();

			
			if( baralho2.getCartas().size() > 0){
				if(nCartasHeroi2 >= maxCartasMao){
					movimentos = jogador2.processarTurno(mesa, null, cloneMovimentos2);
					comprarCarta(); // carta descartada
				}
				else
					movimentos = jogador2.processarTurno(mesa, comprarCarta(), cloneMovimentos2);
			}
			else{
				imprimir("Fadiga: O Herói 2 recebeu "+noCardDmgCounter2+" de dano por falta de cartas no baralho. (Vida restante: "+(vidaHeroi2-noCardDmgCounter2)+").");
				vidaHeroi2 -= noCardDmgCounter2++;
				if( vidaHeroi2 <= 0){
					// Vitoria do jogador 1
					imprimir("O jogador 1 venceu porque o jogador 2 recebeu um dano mortal por falta de cartas ! (Dano : " +(noCardDmgCounter2-1)+ ", Vida Herói 2: "+vidaHeroi2 +")");
					return 1;
				}
				movimentos = jogador2.processarTurno(mesa, null, cloneMovimentos2);
			}
			
			endTime = System.nanoTime();
			totalTime = endTime - startTime;
			if( tempoLimitado!=0 && totalTime > 3e8){ // 300ms
				// Limite de tempo pelo jogador 2. Vitoria do jogador 1.
				return 1;
			}
			else
				imprimir("Tempo usado em processarTurno(): "+totalTime/1e6+"ms");
			
			this.poderHeroicoUsado = false;
            this.lacaiosAtacaramID = new HashSet<Integer>();
			for(int i = 0; i < movimentos.size(); i++){
				processarJogada (movimentos.get(i));
			}
			if( vidaHeroi1 <= 0){
				// Vitoria do jogador 2
				return 2;
			}
			turno++;
		}
		
		// Nunca vai chegar aqui dependendo do número de rodadas
		imprimir("Erro: A partida não pode ser determinada em mais de 60 rounds. Provavel BUG.");
		throw new LamaException(-1, null, "Erro desconhecido. Mais de 60 turnos sem termino do jogo.", 0);
	}

	protected void processarJogada(Jogada umaJogada) throws LamaException {
		int mana = (jogador == 1) ? mesa.getManaJog1(): mesa.getManaJog2();
		
		switch(umaJogada.getTipo()){
		case ATAQUE:
			// TODO: Um ataque foi realizado... quem atacou? quem foi atacado? qual o dano? o alvo morreu ou ficou com quanto de vida? Trate o caso do herói como alvo também.
			//ataque ao Herói
			
			int lacaioIDA = umaJogada.getCartaJogada().getID();
			
			try{
				//pegando a cópia oculta
				CartaLacaio lacaioJogado= (CartaLacaio) lacaios.get(lacaios.indexOf(umaJogada.getCartaJogada()));
				
				//verifica se tem Funcionalidade PROVOCAR ativa
				if (funcionalidadesAtivas.contains(Funcionalidade.PROVOCAR)){
					//verifica se tem algum lacaio com PROVOCAR
					CartaLacaio provocar = pegaProvocar();
					
					if(provocar != null && provocar.equals(umaJogada.getCartaAlvo()) == false ){
						String erroMensagem = "Erro: Tentou-se atacar a carta_ID="+lacaioIDA+", porém tem um lacaio com provocar no campo de ID:"+provocar.getID();
						imprimir(erroMensagem);
						// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
						throw new LamaException(13, umaJogada, erroMensagem, jogador==1?2:1);
					}
				}
				
				//verifica se o lacaio já atacou no turno e está atacando novamente
				if (lacaiosAtacaramID.contains(lacaioIDA) && lacaioJogado.getEfeito() != TipoEfeito.ATAQUE_DUPLO){
					String erroMensagem = "Erro: Tentou-se atacar mais de uma vez com a carta_id="+lacaioIDA+", porém esta carta não pode atacar nesse turno\n";
					imprimir(erroMensagem);
					// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
					throw new LamaException(7, umaJogada, erroMensagem, jogador==1?2:1);
				}
				
				else if ((lacaioJogado.getEfeito() == TipoEfeito.ATAQUE_DUPLO)){
					int[] lacaioAtaqueDuplo = new int [lacaios.size()];
					
					if (lacaioAtaqueDuplo[lacaios.indexOf(lacaioJogado)] == 2){
						String erroMensagem = "Erro: Tentou-se atacar mais de uma vez com a carta_id="+lacaioIDA+", porém esta carta não pode atacar nesse turno\n";
						imprimir(erroMensagem);
						// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
						throw new LamaException(7, umaJogada, erroMensagem, jogador==1?2:1);
					}
					else {
						lacaioAtaqueDuplo[lacaios.indexOf(lacaioJogado)]++;
					}
				}
				
				//verifica se o lacaio tentou atacar nesse turno e não tem investida para a ação
				if (lacaioJogado.getTurno() == turno && lacaioJogado.getEfeito() != TipoEfeito.INVESTIDA){
					String erroMensagem = "Erro: Tentou-se usar a carta_id="+lacaioIDA+", porém esta carta não pode atacar nesse turno\n";
					imprimir(erroMensagem);
					// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
					throw new LamaException(6, umaJogada, erroMensagem, jogador==1?2:1);
				}
				
				//atacando o herói diretamente
				if (umaJogada.getCartaAlvo() == null){
					//log da jogada
					imprimiLog(umaJogada);
					//atualiza a vida do herói
					atualizaVidaHeroi(lacaioJogado.getAtaque());
					//retira lacaios mortos do campo
					lacaiosMortosOpo();
					lacaiosMortosMeus();
					break;
				}
				else{
					//pegando a cópia oculta
					try{
						CartaLacaio lacaioAlvo = (CartaLacaio) lacaiosOponente.get(lacaiosOponente.indexOf(umaJogada.getCartaAlvo()));
						
						//log da jogada
						imprimiLog(umaJogada);
						
						//atualiza a vida dos lacaios
						lacaioAlvo.setVidaAtual(lacaioAlvo.getVidaAtual() - lacaioJogado.getAtaque());
						lacaioJogado.setVidaAtual(lacaioJogado.getVidaAtual() - lacaioAlvo.getAtaque());	
						
						//adiciona na lista de quem ja atacou neste turno
						if (lacaioJogado.getVidaAtual() > 0){
							lacaiosAtacaramID.add(lacaioIDA);
						}
						
						//retira lacaios mortos do campo
						lacaiosMortosOpo();
						lacaiosMortosMeus();
						
					} catch (ArrayIndexOutOfBoundsException e){
						String erroMensagem = "Erro: Tentou-se atacar a carta_id="+ umaJogada.getCartaAlvo().getID()+", porém esse lacaio é inválido\n";
						imprimir(erroMensagem);
						// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
						throw new LamaException(8, umaJogada, erroMensagem, jogador==1?2:1);
					}
				}
				
			} catch (ArrayIndexOutOfBoundsException e){
				String erroMensagem = "Erro: Tentou-se atacar a carta_id="+ umaJogada.getCartaAlvo().getID()+", porém esse lacaio é inválido\n";
				imprimir(erroMensagem);
				// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
				throw new LamaException(5, umaJogada, erroMensagem, jogador==1?2:1);
			}
			
			break;
		case LACAIO:
			int lacaioID = umaJogada.getCartaJogada().getID();
			imprimir("JOGADA: O lacaio_id="+lacaioID+" foi baixado para a mesa.");
			
			//verifica se estou invocando uma cartaMagia como Lacaio
			if (umaJogada.getCartaJogada().getClass() == CartaMagia.class){
				String erroMensagem = "Erro:Tentou invocar a carta ID:"+umaJogada.getCartaJogada().getID()+"nome:"+umaJogada.getCartaJogada().getNome()+" de forma incorreta\n";
				imprimir(erroMensagem);
				// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
				throw new LamaException(3, umaJogada, erroMensagem, jogador==1?2:1);
			}
			
			//verifica se tem mana o suficiente para realizar a jogada
			if (umaJogada.getCartaJogada().getMana() > mana ){
				String erroMensagem = "Erro:Tentou fazer uma invocação Tipo:"+umaJogada.getTipo()+" porém essa carta custa "+umaJogada.getCartaJogada().getMana()+" e o jogador possui "+mana+" de mana\n";
				imprimir(erroMensagem);
				// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
				throw new LamaException(2, umaJogada, erroMensagem, jogador==1?2:1);
			}
			else{
				mana -= umaJogada.getCartaJogada().getMana();
			}
			
			//verifica se temos 7 lacaios no campo
			if (lacaios.size() == 7){
				String erroMensagem = "Erro: Tentou-se usar a carta_id="+lacaioID+" de nome "+umaJogada.getCartaJogada().getNome()+", porém já temos 7 lacaios me campo. IDs de cartas na mao:\n";
				for(Carta card : lacaios){
					erroMensagem += card.getID() + ", ";
				}
				imprimir(erroMensagem);
				// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
				throw new LamaException(4, umaJogada, erroMensagem, jogador==1?2:1);
			}
			
			//verifica se existe a carta na mão
			if(mao.contains(umaJogada.getCartaJogada())){
				Carta lacaioBaixado = mao.get(mao.indexOf(umaJogada.getCartaJogada()));
				ArrayList<Carta> maoLacaios = new ArrayList<Carta>();
				
				//um vetor com apenas os lacaios da mao
				for(int i = 0; i <mao.size(); i++){
					if (mao.get(i).getClass()== CartaLacaio.class){
						maoLacaios.add(mao.get(i));
					}
				}
			
				//verifica se houve alteracao na carta
				CartaLacaio realI =(CartaLacaio) (maoLacaios.stream()
						.filter(p -> ((CartaLacaio) p).getID()== lacaioBaixado.getID())
						.findFirst()
						.orElse(null));

				if ( realI != null && comparaLacaios(realI, (CartaLacaio)lacaioBaixado) != true ){
					String erroMensagem = "Erro: Tentou-se atacar com a carta_id="+lacaioBaixado.getID()+", porém esse lacaio é inválido";
					imprimir(erroMensagem);
					// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
					throw new LamaException(5, umaJogada, erroMensagem, jogador==1?2:1);
				}
				
				imprimiLog(umaJogada);//log da jogada
				lacaios.add(lacaioBaixado); // lacaio adicionado à mesa
				mao.remove(umaJogada.getCartaJogada()); // lacaio retirado da mão
			}
			else{
				String erroMensagem = "Erro: Tentou-se usar a carta_id="+lacaioID+", porém esta carta não existe na mao. IDs de cartas na mao:\n";
				for(Carta card : mao){
					erroMensagem += card.getID() + ", ";
				}
				imprimir(erroMensagem);
				// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
				throw new LamaException(1, umaJogada, erroMensagem, jogador==1?2:1);
			}
			
			// Obs: repare que este código funcionará tanto quando trata-se do turno do jogador1 ou do jogador2.
			break;
		case MAGIA:
			// TODO: Uma magia foi usada... é de área, alvo ou buff? Se de alvo ou buff, qual o lacaio receberá o dano/buff ?
			int magiaID = umaJogada.getCartaJogada().getID();
			imprimir("JOGADA: A magia_id="+magiaID+" foi baixado para a mesa.");
			
			//verifica se estou invocando uma cartaLacaio como Magia
			if (umaJogada.getCartaJogada().getClass() == CartaLacaio.class){
				String erroMensagem = "Erro:Tentou invocar a carta ID:"+umaJogada.getCartaJogada().getID()+"nome:"+umaJogada.getCartaJogada().getNome()+" de forma incorreta\n";
				imprimir(erroMensagem);
				// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
				throw new LamaException(9, umaJogada, erroMensagem, jogador==1?2:1);
			}
			
			//verifica se tem mana o suficiente para realizar a jogada
			if (umaJogada.getCartaJogada().getMana() > mana ){
				String erroMensagem = "Erro:Tentou fazer uma invocação Tipo:"+umaJogada.getTipo()+"porém essa carta custa"+umaJogada.getCartaJogada().getMana()+"e o jogador possui"+mana+"de mana\n";
				imprimir(erroMensagem);
				// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
				throw new LamaException(2, umaJogada, erroMensagem, jogador==1?2:1);
			}
			else{
				mana -= umaJogada.getCartaJogada().getMana();
			}
				
			//verifica se existe a carta na mão
			if(mao.contains(umaJogada.getCartaJogada())){
				//pegando a cópia oculta
				CartaMagia  magiaUsada = (CartaMagia) mao.get(mao.indexOf(umaJogada.getCartaJogada()));
				switch(magiaUsada.getMagiaTipo()){
					case ALVO:
						//caso seja um herói
						if (umaJogada.getCartaAlvo() == null){
							//log da jogada
							imprimiLog(umaJogada);
							//usa a magia diretamente contra o Herói
							atualizaVidaHeroi( magiaUsada.getMagiaDano());
							
							//retira lacaios mortos do campo
							lacaiosMortosOpo();
							lacaiosMortosMeus();
							
							break;
						}
						else{//caso seja um lacaio
							//pegando a cópia oculta
							try{
								CartaLacaio lacaioAlvo = (CartaLacaio) lacaiosOponente.get(lacaiosOponente.indexOf(umaJogada.getCartaAlvo()));
								
								//verifico se é um lacaio meu
								CartaLacaio lacaioMeu = (CartaLacaio) lacaios.stream()
										.filter(p -> p.getID() == lacaioAlvo.getID())
										.findFirst()
										.orElse(null);
								
								//atacando um lacaio inválido, meu lacaio
								if (lacaioAlvo.equals(lacaioMeu)){
									String erroMensagem = "Erro: Tentou-se usar a magia_id="+magiaID+"do tipo"+ magiaUsada.getMagiaTipo() +"em um lacaio inválido"+lacaioAlvo.getID();
									imprimir(erroMensagem);
									// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
									throw new LamaException(10, umaJogada, erroMensagem, jogador==1?2:1);
								}
								
								//log da jogada
								imprimiLog(umaJogada);
								//retira vida do lacaio
								lacaioAlvo.setVidaAtual(lacaioAlvo.getVidaAtual() - magiaUsada.getMagiaDano());
								//retira lacaios mortos do campo
								lacaiosMortosOpo();
								lacaiosMortosMeus();
								
							} catch (ArrayIndexOutOfBoundsException e){
								String erroMensagem = "Erro: Tentou-se atacar a carta_id="+ umaJogada.getCartaAlvo().getID()+", porém esse lacaio é inválido\n";
								imprimir(erroMensagem);
								// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
								throw new LamaException(5, umaJogada, erroMensagem, jogador==1?2:1);
							}
						}
						break;
					case AREA:
						//log da jogada
						imprimiLog(umaJogada);
						int i = 0;
						while( i < lacaiosOponente.size()){
							CartaLacaio lacaioAlvo = (CartaLacaio)lacaiosOponente.get(i);
							
							//procura se o alvo é um lacaio do meu campo
							CartaLacaio lacaioMeu = (CartaLacaio) lacaios.stream()
									.filter(p -> p.getID() == lacaioAlvo.getID())
									.findFirst()
									.orElse(null);
							
							//atacando um lacaio inválido, o do oponente
							if (lacaioAlvo.equals(lacaioMeu)){
								String erroMensagem = "Erro: Tentou-se usar a magia_id="+magiaID+"do tipo"+ magiaUsada.getMagiaTipo() +"em um lacaio inválido"+lacaioAlvo.getID();
								imprimir(erroMensagem);
								// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
								throw new LamaException(10, umaJogada, erroMensagem, jogador==1?2:1);
							}
							
							lacaioAlvo.setVidaAtual(lacaioAlvo.getVidaAtual() - magiaUsada.getMagiaDano());
							i++;
						}
						
						//usa a magia diretamente contra o Herói
						atualizaVidaHeroi( magiaUsada.getMagiaDano());
						//retira lacaios mortos do campo
						lacaiosMortosOpo();
						lacaiosMortosMeus();
						break;
					case BUFF:
						if (lacaiosOponente.contains(umaJogada.getCartaAlvo()) == false ){
							//pegando a cópia oculta
							CartaLacaio lacaioAlvo = (CartaLacaio) mao.get(mao.indexOf(umaJogada.getCartaAlvo()));
							
							//verifica se o alvo é um herói
							if (lacaioAlvo == null ){
								String erroMensagem = "Erro: Tentou-se usar a magia_id="+magiaID+"do tipo"+ magiaUsada.getMagiaTipo() +"em um Herói "+jogador+"\n";
								imprimir(erroMensagem);
								// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
								throw new LamaException(10, umaJogada, erroMensagem, jogador==1?2:1);
							}
							
							//procura se o alvo é um lacaio oponente
							CartaLacaio lacaioOpo = (CartaLacaio) lacaiosOponente.stream()
									.filter(p -> p.getID() == lacaioAlvo.getID())
									.findFirst()
									.orElse(null);
							
							//atacando um lacaio inválido, o do oponente
							if (lacaioAlvo.equals(lacaioOpo)){
								String erroMensagem = "Erro: Tentou-se usar a magia_id="+magiaID+"do tipo"+ magiaUsada.getMagiaTipo() +"em um lacaio inválido"+lacaioAlvo.getID();
								imprimir(erroMensagem);
								// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
								throw new LamaException(10, umaJogada, erroMensagem, jogador==1?2:1);
							}
							//log da jogada
							imprimiLog(umaJogada);
							//atualiza a vida e o ataque
							lacaioAlvo.setVidaAtual(lacaioAlvo.getVidaAtual() + magiaUsada.getMagiaDano());
							lacaioAlvo.setAtaque(lacaioAlvo.getAtaque() + magiaUsada.getMagiaDano());
							
						}
						else{
							String erroMensagem = "Erro: Tentou-se atacar a carta_id="+ umaJogada.getCartaAlvo().getID()+", porém esse lacaio é inválido\n";
							for(Carta card : mao){
								erroMensagem += card.getID() + ", ";
							}
							imprimir(erroMensagem);
							// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
							throw new LamaException(8, umaJogada, erroMensagem, jogador==1?2:1);
						}
						break;
				}
			}
			else{
				String erroMensagem = "Erro: Tentou-se usar a magia_id="+magiaID+", porém esta carta não existe na mao. IDs de cartas na mao:\n";
				for(Carta card : mao){
					erroMensagem += card.getID() + ", ";
				}
				imprimir(erroMensagem);
				// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
				throw new LamaException(1, umaJogada, erroMensagem, jogador==1?2:1);
			}
			break;
		case PODER:
			// TODO: O poder heroico foi usado. Qual o alvo ?
			//pegando a cópia oculta
			CartaLacaio lacaioAlvo = (CartaLacaio) lacaiosOponente.get(lacaiosOponente.indexOf(umaJogada.getCartaAlvo()));
			
			//verifica se o poderHeroico já foi usado
			if (poderHeroicoUsado == true){
				String erroMensagem = "Erro: Tentou-se usar o poder heróico novamente na carta_id_alvo="+lacaioAlvo.getID();
				imprimir(erroMensagem);
				// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
				throw new LamaException(11, umaJogada, erroMensagem, jogador==1?2:1);
			}
			
			//verifica se tem mana o suficiente para realizar a jogada
			if (2 > mana){
				String erroMensagem = "Erro:Tentou fazer uma invocação Tipo:"+umaJogada.getTipo()+"porém essa carta custa"+umaJogada.getCartaJogada().getMana()+"e o jogador possui"+mana+"de mana\n";
				imprimir(erroMensagem);
				// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
				throw new LamaException(2, umaJogada, erroMensagem, jogador==1?2:1);
			}
			else{
				mana -= 2;
			}
			
			//verifica se tem Funcionalidade PROVOCAR ativa
			if (funcionalidadesAtivas.contains(Funcionalidade.PROVOCAR)){
				//verifica se tem algum lacaio com PROVOCAR
				CartaLacaio provocarH = pegaProvocar();
				
				if(provocarH != null && provocarH.equals((CartaLacaio)umaJogada.getCartaAlvo()) == false ){
					String erroMensagem = "Erro: Tentou-se atacar a carta_ID="+lacaioAlvo.getID()+", porém tem um lacaio com provocar no campo de ID:"+provocarH.getID();
					imprimir(erroMensagem);
					// Dispara uma LamaException passando o código do erro, uma mensagem descritiva correspondente e qual jogador deve vencer a partida.
					throw new LamaException(13, umaJogada, erroMensagem, jogador==1?2:1);
				}
			}
			
			//log da jogada
			imprimiLog(umaJogada);
			//verifica se ataca o herói inimigo ou um lacaio
			if (lacaioAlvo == null){
				atualizaVidaHeroi(1);
			}
			else{
				atualizaVidaHeroi(lacaioAlvo.getAtaque());
				lacaioAlvo.setVidaAtual(lacaioAlvo.getVidaAtual() -1);
			}
			
			//seta poder usado
			poderHeroicoUsado = true;
			
			//retira lacaios mortos do campo
			lacaiosMortosOpo();
			lacaiosMortosMeus();
			
			break;
		default:
			break;
		}
		
		//atualiza a mana da mesa
		atualizaMana(mana, mesa);
		
		return;
	}
	
	private Carta comprarCarta(){
		if(this.jogador == 1){
			if(baralho1.getCartas().size() <= 0)
				throw new RuntimeException("Erro: Não há mais cartas no baralho para serem compradas.\n");
			Carta nova = baralho1.comprarCarta();
			mao.add(nova);
			nCartasHeroi1++;
			return (Carta) UnoptimizedDeepCopy.copy(nova);
		}
		else{
			if(baralho2.getCartas().size() <= 0)
				throw new RuntimeException("Erro: Não há mais cartas no baralho para serem compradas.\n");
			Carta nova = baralho2.comprarCarta();
			mao.add(nova);
			nCartasHeroi2++;
			return (Carta) UnoptimizedDeepCopy.copy(nova);
		}
	}
	
	 /**
	  * atualiza a mana gasta do jogador no turno
	  * 
	  * @param mana   quantidade de mana atualizada
	  * @param mesa   mesa da jogada
	  * @return       seta a mana do jogador do turno
	  * @author       Matheus Mendes Araujo
	  */
	private void atualizaMana(int mana, Mesa mesa){
		if (jogador == 1){
			mesa.setManaJog1(mana);
		}
		else{
			mesa.setManaJog2(mana);
		}
	}
	
	/**
	  * retorna a vida do Herói
	  * 
	  * @param jogador o jogador que quero saber a vida
	  * @return       a vida do jogador
	  * @author       Matheus Mendes Araujo
	  */
	private int retornaVidaHeroi(int jogador){
		if (jogador == 1){
			return vidaHeroi1;
		}
		else{
			return vidaHeroi2;
		}
	}
	
	/**
	  * atualiza a vida do Herói
	  * 
	  * @param qtde   quantidade de dano para ser retirado
	  * @return       seta a mana do jogador do turno
	  * @author       Matheus Mendes Araujo
	  */
	private void atualizaVidaHeroi(int dano){
		if (jogador == 1){
			vidaHeroi1 -= dano;
		}
		else{
			vidaHeroi2 -= dano;
		}
	}
	
	/**
	  * verifica se teve alguma alteração na jogada pelo usuário
	  * 
	  * @param real   carta vindo da cópia oculta para validar
	  * @param jogada carta jogada que pode ter sido alterada
	  * @return       falso, se teve alteração
	  * 			  verdadeiro, se são iguais
	  * @author       Matheus Mendes Araujo
	  */
	private boolean comparaLacaios(CartaLacaio real, CartaLacaio jogada){
		
		if (real.getAtaque() != jogada.getAtaque()){
			return false;
		}
		else if (real.getClass() != jogada.getClass()){
			return false;
		}
		else if (real.getEfeito() != jogada.getEfeito()){
			return false;
		}
		else if (real.getID() != jogada.getID()){
			return false;
		}
		else if (real.getMana() != jogada.getMana()){
			return false;
		}
		else if (real.getNome() != jogada.getNome()){
			return false;
		}
		else if (real.getTurno() != jogada.getTurno()){
			return false;
		}
		else if (real.getVidaAtual() != jogada.getVidaAtual()){
			return false;
		}
		else if (real.getVidaMaxima() != jogada.getVidaMaxima()){
			return false;
		}
		
		return true;
	} 
	
	/**
	  * verifica se tem lacaios Mortos
	  *
	  * @return       atualiza o array dos lacaiosOponente
	  * @author       Matheus Mendes Araujo
	 * @return 
	  */
	private void lacaiosMortosOpo(){
		lacaiosOponente = (ArrayList<Carta>) lacaiosOponente.stream()
				.filter(p -> ((CartaLacaio)p).getVidaAtual() > 0)
				.collect(Collectors.toList());
		
		for (int i = 0; i < lacaiosOponente.size(); i++){
			lacaiosOponente.get(i).toString();
		}
	}

	/**
	  * procura lacaios com provocar
	  *
	  * @return       lacaio com provocar
	  * @author       Matheus Mendes Araujo
	  */
	private CartaLacaio pegaProvocar(){
		
		CartaLacaio provocar =(CartaLacaio) (lacaios.parallelStream()
				.filter(p -> ((CartaLacaio) p).getEfeito().equals(TipoEfeito.PROVOCAR))
				.findFirst()
				.orElse(null));
		
		
		return provocar;
	}
	
	/**
	  * verifica se tem lacaios Mortos
	  *
	  * @return       atualiza o array dos meus lacaios
	  * @author       Matheus Mendes Araujo
	  */
	private void lacaiosMortosMeus(){
		lacaios = (ArrayList<Carta>) lacaios.stream()
				.filter(p -> ((CartaLacaio)p).getVidaAtual() > 0)
				.collect(Collectors.toList());
		
	}
	
	/**
	  * imprimi log da logada
	  *
	  * @param umaJogada   Através da jogada, eu posso realizar a impressão do que aconteceu
	  * @author       Matheus Mendes Araujo
	  */
	private void imprimiLog(Jogada umaJogada) throws LamaException {
		switch(umaJogada.getTipo()){
			case ATAQUE:
				CartaLacaio lacaioAlvoA = (CartaLacaio) umaJogada.getCartaAlvo();
				if (lacaioAlvoA  == null){
					imprimir("JOGADA: Um ataque do lacaio_id ="+umaJogada.getCartaJogada().getID()+" ("+umaJogada.getCartaJogada().getNome()+") no lacaio_id =Heroi"+(jogador==1?2:1));
					imprimir("O heroi "+(jogador==1?2:1)+" tomou "+((CartaLacaio) umaJogada.getCartaJogada()).getAtaque()+" de dano (vida restante:"+(retornaVidaHeroi(jogador)-((CartaLacaio) umaJogada.getCartaJogada()).getAtaque())+").");	
				}
				else{
					imprimir ("JOGADA: Um ataque do lacaio_id ="+umaJogada.getCartaJogada().getID()+" ("+umaJogada.getCartaJogada().getNome()+") no lacaio_id ="+lacaioAlvoA.getID()+" ("+lacaioAlvoA.getNome()+").");
					
				}
				break;
				
			case LACAIO:
				imprimir ("JOGADA: Um lacaio entrou na mesa, lacaio_id="+umaJogada.getCartaJogada().getID()+"("+ umaJogada.getCartaJogada().getNome()+")");
				break;
				
			case MAGIA:
				CartaMagia  magiaUsada = (CartaMagia) umaJogada.getCartaJogada();
				CartaLacaio lacaioAlvo = (CartaLacaio) umaJogada.getCartaAlvo();
				switch(magiaUsada.getMagiaTipo()){
					case ALVO:
						imprimir ("JOGADA: Uma magia usada de id="+magiaUsada.getID()+" de alvo, com dano de "+magiaUsada.getMagiaDano());
						if (lacaioAlvo != null){
							if (lacaioAlvo.getVidaAtual() -1 > 0){
								imprimir ("Lacaio id="+lacaioAlvo.getID()+"("+lacaioAlvo.getNome()+") sofreu dano, mas esta vivo (vida antes="+lacaioAlvo.getVidaAtual()+". dano="+magiaUsada.getMagiaDano()+". vida agora="+(lacaioAlvo.getVidaAtual()-magiaUsada.getMagiaDano())+"). ");
							}
							else{
								imprimir ("Lacaio id="+lacaioAlvo.getID()+"("+lacaioAlvo.getNome()+") sofreu dano mas morreu (vida antes="+lacaioAlvo.getVidaAtual()+". dano="+magiaUsada.getMagiaDano()+". vida agora="+(lacaioAlvo.getVidaAtual()-magiaUsada.getMagiaDano())+").");
							}
						}
						else {
							imprimir ("HEROI"+(jogador==1?1:2)+"recebeu " +magiaUsada.getMagiaDano()+" de dano(vida agora="+(retornaVidaHeroi(jogador)-magiaUsada.getMagiaDano())+").");
						}
						break;
					case AREA:
						imprimir ("JOGADA: Uma magia usada de id="+magiaUsada.getID()+" de area.");
						
						int i = 0;
						while( i < lacaiosOponente.size()){
							lacaioAlvo = (CartaLacaio)lacaiosOponente.get(i);
							
							if (lacaioAlvo.getVidaAtual() -1 > 0){
								imprimir ("Lacaio id="+lacaioAlvo.getID()+"("+lacaioAlvo.getNome()+") sofreu dano, mas esta vivo (vida antes="+lacaioAlvo.getVidaAtual()+". dano="+magiaUsada.getMagiaDano()+". vida agora="+(lacaioAlvo.getVidaAtual()-magiaUsada.getMagiaDano())+"). ");
							}
							else{
								imprimir ("Lacaio id="+lacaioAlvo.getID()+"("+lacaioAlvo.getNome()+") sofreu dano mas morreu (vida antes="+lacaioAlvo.getVidaAtual()+". dano="+magiaUsada.getMagiaDano()+". vida agora="+(lacaioAlvo.getVidaAtual()-magiaUsada.getMagiaDano())+").");
							}
							i++;
						}
						
						imprimir ("HEROI"+(jogador==1?1:2)+"recebeu " +magiaUsada.getMagiaDano()+" de dano(vida agora="+(retornaVidaHeroi(jogador)-magiaUsada.getMagiaDano())+").");
						
						break;
					case BUFF:
						imprimir ("JOGADA: Uma magia usada de id="+magiaUsada.getID()+" de buff.");
						imprimir ("Lacaio id="+ umaJogada.getCartaAlvo().getID()
								+" ("+umaJogada.getCartaAlvo().getNome()+") "
								+ "buffado +"+magiaUsada.getMagiaDano()+"/+"+magiaUsada.getMagiaDano()
								+ "(antes="+lacaioAlvo.getAtaque()+"/"+lacaioAlvo.getVidaAtual() +"."
								+ " buff=+"+magiaUsada.getMagiaDano()+"/+"+magiaUsada.getMagiaDano() +". "
								+ "Agora="+(lacaioAlvo.getAtaque()+magiaUsada.getMagiaDano())
								+"/"+(lacaioAlvo.getVidaAtual()+magiaUsada.getMagiaDano() )+".)");
						break;
				}
				break;
				
			case PODER:
				if (umaJogada.getCartaAlvo() != null){
					CartaLacaio lacaioPoder = (CartaLacaio) umaJogada.getCartaAlvo();
					imprimir ("JOGADA: Um poder heroico usado no lacaio_id="+lacaioPoder.getID());
					if (lacaioPoder.getVidaAtual() -1 > 0){
						imprimir ("Lacaio id="+lacaioPoder.getID()+"("+lacaioPoder.getNome()+") sofreu dano, mas esta vivo (vida antes="+lacaioPoder.getVidaAtual()+". dano=1. vida agora="+(lacaioPoder.getVidaAtual()-1)+"). ");
					}
					else{
						imprimir ("Lacaio id="+lacaioPoder.getID()+"("+lacaioPoder.getNome()+") sofreu dano mas morreu (vida antes="+lacaioPoder.getVidaAtual()+". dano=1. vida agora="+(lacaioPoder.getVidaAtual()-1)+").");
					}
					imprimir("O heroi "+(jogador==1?1:2)+" tomou "+lacaioPoder.getAtaque()+" de dano (vida restante:"+(retornaVidaHeroi(jogador)-lacaioPoder.getAtaque())+").");	
				}
				else
					imprimir ("JOGADA: Um poder heroico usado no Heroi "+(jogador==1?2:1));
				break;
			
		}
	}
}
